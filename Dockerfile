FROM bash:latest

RUN wget -O /bin/kubectl https://storage.googleapis.com/kubernetes-release/release/$(wget -q -O- https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
RUN chmod +x /bin/kubectl

COPY backup.sh /usr/bin/backup.sh
RUN chmod +x /usr/bin/backup.sh

CMD ["/usr/bin/backup.sh"]
