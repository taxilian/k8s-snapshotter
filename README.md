# k8s Snapshotter

## Create and retain a certain number of kubernetes volume snapshots using a CronJob

## Available Tags

| Tag        |                                  Description |
|:-----------|---------------------------------------------:|
| alpha      |                Most recently updated version |
| alpha-v0.1 |                        Latest tagged release |
| latest     | Kept for compatibility with old instructions |

## Installation

All that is needed to prepare is to create a service account to manage volumesnapshots:

```yml
# snapshotter.yml
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: snapshotter
---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: snapshotter
rules:
  - apiGroups: [snapshot.storage.k8s.io]
    resources: [volumesnapshots]
    verbs: [get, list, create, delete]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: snapshotter
subjects:
  - kind: ServiceAccount
    name: snapshotter
roleRef:
  kind: Role
  name: snapshotter
  apiGroup: rbac.authorization.k8s.io
```

```bash
kubectl apply -f snapshotter.yml
```

## **Features**

## Create snapshots using a CronJob

```yml
---
# my-volume-backup.yml
apiVersion: batch/v1
kind: CronJob
metadata:
  name: my-volume-daily-snapshot
spec:
  schedule: '0 8 * * *'
  concurrencyPolicy: Replace
  jobTemplate:
    spec:
      backoffLimit: 1
      template:
        spec:
          serviceAccountName: snapshotter
          restartPolicy: Never
          containers:
            - name: snapshotter
              image: registry.gitlab.com/tygrdev/k8s-snapshotter:alpha-v0.1
              env:
                - name: PVC
                  value: my-volume-claim
                - name: FREQUENCY
                  value: Daily
                - name: RETAIN
                  value: '7'
                - name: DATE_FORMAT
                  value: '%Y-%m-%d'
```

```bash
kubectl apply -f my-volume-backup.yml
```

You may create multiple CronJobs for a single persistent volume claim.

## Manually trigger a snapshot

```yml
---
# my-volume-backup-manual.yml
apiVersion: batch/v1
kind: Job
metadata:
  name: my-volume-manual-snapshot
spec:
  backoffLimit: 1
  template:
    spec:
      serviceAccountName: snapshotter
      restartPolicy: Never
      containers:
        - name: snapshotter
          image: registry.gitlab.com/tygrdev/k8s-snapshotter:alpha-v0.1
          env:
            - name: PVC
              value: my-volume-claim
            - name: FREQUENCY
              value: Manual
            - name: RETAIN
              value: '3'
            - name: DATE_FORMAT
              value: '%Y-%m-%d-%H.%M.%S'
```

```bash
kubectl replace --force -f my-volume-backup-manual.yml
```

Although the replace command is used, snapshotter will still retain however many backups specified by RETAIN. The thing being replaced is the 'Job' that creates the snapshot.

## Restore previous state of a volume

First, list all available snapshots made by snapshotter:

```bash
kubectl get volumesnapshots -l pvc=my-volume-claim
```

Then replace the existing volume claim with one generated from one of the snapshots:

```yml
---
# my-volume-restore.yml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: my-volume-claim
spec:
  dataSource:
    # Use the name of the VolumeSnapshot you'd like to restore from
    name: my-volume-claim-2021-01-10
    kind: VolumeSnapshot
    apiGroup: snapshot.storage.k8s.io
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 2Gi # Must match snapshot restore size
  storageClassName: do-block-storage # For digital ocean
```

```bash
kubectl replace --force -f my-volume-restore.yml
```

You will need to delete all pods the volume is attached to. Afterwards, the replacement pods will be pending while the volume is created from the snapshot. There will be a service interruption during this time.

## Environment Variables

Add these env variables to the container spec for `registry.gitlab.com/tygrdev/k8s-snapshotter:alpha-v0.1`. All are required

| name        | description                                                                                                                                           |
| ----------- | ----------------------------------------------------------------------------------------------------------------------------------------------------- |
| PVC         | Persistent volume claim to make snapshots for                                                                                                         |
| FREQUENCY   | A label to differentiate multiple types of backups for the same volume. Ex: 'Daily', 'Weekly', and 'Manual'. This can be any valid label name |
| RETAIN      | Number of backups, of this frequency, to retain at one time. Must be more than one. Older backups will be removed before the creation of another one. |
| DATE_FORMAT | bash date format string to be appended to the name of the snapshot. The pattern must produce unique values for each run. For example, if the pattern is %Y-%m-%d, only one can be created each day                 |

## Digital Ocean

For digital ocean, you are limited to create one snapshot every ten minutes and at most 25 snapshots per volume. For those reasons I recommend the following staggered scheme for 3AM EST backups:

| FREQUENCY | Cron Expression | DATE_FORMAT         | RETAIN |
| --------- | --------------- | ------------------- | -----: |
| Daily     | `0 8 * * *`     | `%Y-%m-%d`          |      7 |
| Weekly    | `15 8 * * SUN`  | `%Y-wk%U`           |      4 |
| Monthly   | `30 8 1 * *`    | `%Y-%m`             |     12 |
| Manual    |                 | `%Y-%m-%d-%H.%M.%S` |      2 |
| Total     |                 |                     |     25 |
